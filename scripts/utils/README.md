# Utils helpers scripts

- utils/log/
    - [utils/log/debug.sh](#utilslogdebugsh)
    - [utils/log/error.sh](#utilslogerrorsh)
    - [utils/log/info.sh](#utilsloginfosh)
    - [utils/log/warn.sh](#utilslogwarnsh)
- [utils/get_secret_variable.sh](#utilsget_secret_variablesh)
- [utils/require_secret_variable.sh](#utilsrequire_secret_variablesh)
- [utils/require_variable.sh](#utilsrequire_variablesh)

## utils/log/debug.sh

Print a debug message.

### Dependencies

- date

### Environment variables

| Variable              | Required | Description                                                                                                                         |
|:----------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------|
| `CIH_LOG_DATE_FORMAT` | No       | Custom date format four output log<br>See formats [here](http://man7.org/linux/man-pages/man1/date.1.html)<br>Defaults to `+%FT%TZ` |

### Arguments

Get log message as first argument

### Output

Print formated log message with date

### Exit codes

| Code   | Description                                           |
|:-------|:------------------------------------------------------|
| `0`    | Success                                               |
| `> 0`  | Failure                                               |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/log/debug.sh "my debug message"
```

## utils/log/error.sh

Print an error message and exit.

### Dependencies

- date

### Environment variables

| Variable              | Required | Description                                                                                                                         |
|:----------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------|
| `CIH_LOG_DATE_FORMAT` | No       | Custom date format four output log<br>See formats [here](http://man7.org/linux/man-pages/man1/date.1.html)<br>Defaults to `+%FT%TZ` |

### Arguments

Get log message as first argument

### Output

Print formated log message with date

### Exit codes

| Code   | Description |
|:-------|:------------|
| `1`    | Failure     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/log/error.sh "my error message"
```

## utils/log/info.sh

Print an information message.

### Dependencies

- date

### Environment variables

| Variable              | Required | Description                                                                                                                         |
|:----------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------|
| `CIH_LOG_DATE_FORMAT` | No       | Custom date format four output log<br>See formats [here](http://man7.org/linux/man-pages/man1/date.1.html)<br>Defaults to `+%FT%TZ` |

### Arguments

Get log message as first argument

### Output

Print formated log message with date

### Exit codes

| Code   | Description |
|:-------|:------------|
| `0`    | Success     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/log/info.sh "my information message"
```

## utils/log/warn.sh

Print a warning message.

### Dependencies

- date

### Environment variables

| Variable              | Required | Description                                                                                                                         |
|:----------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------|
| `CIH_LOG_DATE_FORMAT` | No       | Custom date format four output log<br>See formats [here](http://man7.org/linux/man-pages/man1/date.1.html)<br>Defaults to `+%FT%TZ` |

### Arguments

Get log message as first argument

### Output

Print formated log message with date

### Exit codes

| Code   | Description |
|:-------|:------------|
| `0`    | Success     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/log/warn.sh "my warning message"
```

## utils/get_secret_variable.sh

Print decoded value of a secret variable.

### Dependencies

- base64

### Environment variables

_Script uses no environment variables_

### Arguments

Get variable name as first argument

### Output

Output decoded variable value

### Exit codes

| Code   | Description |
|:-------|:------------|
| `0`    | Success     |
| `> 0`  | Failure     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/get_secret_variable.sh "MY_VAR"
```

## utils/require_secret_variable.sh

Require a secret variable to be present.

### Dependencies

- base64

### Environment variables

_Script uses no environment variables_

### Arguments

Get variable name as first argument

### Output

Print an error message if variable is not set

### Exit codes

| Code   | Description |
|:-------|:------------|
| `0`    | Success     |
| `> 0`  | Failure     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/require_secret_variable.sh "MY_VAR"
```

## utils/require_variable.sh

Require a variable to be present.

### Dependencies

_Script uses no dependencies variables_

### Environment variables

_Script uses no environment variables_

### Arguments

Get variable name as first argument

### Output

Print an error message if variable is not set

### Exit codes

| Code   | Description |
|:-------|:------------|
| `0`    | Success     |
| `> 0`  | Failure     |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/utils/require_variable.sh "MY_VAR"
```
