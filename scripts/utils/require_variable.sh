#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

var_name="${1}"

if [ -z "$(eval echo \$"${var_name}")" ]; then
  sh "${_CIH_PATH}/utils/log/error.sh" "${var_name} environment variable should be defined"
fi


