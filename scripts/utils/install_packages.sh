#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

if [ ${#} -eq 0 ]; then
  sh "${_CIH_PATH}/utils/log/error.sh" "Please specify at least one package."
fi

if command -v "apk" > /dev/null; then
  apk add --no-cache ${@}
elif command -v "apt" > /dev/null; then
  apt update && apt install -y ${@}
else
  sh "${_CIH_PATH}/utils/log/error.sh" "No or unsupported package manager detected."
fi
