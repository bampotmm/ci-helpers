#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

printf "%s" "${1}" | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | tr '[:upper:]' '[:lower:]'
