#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "DOCKER_PASSWORD"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_USER"

if [ -z "${_KANIKO_DOCKER_CONFIG_PATH}" ]; then
 _KANIKO_DOCKER_CONFIG_PATH="/kaniko/.docker/config.json"
fi

mkdir -p "$(dirname "${_KANIKO_DOCKER_CONFIG_PATH}")"

_TOKEN=$(printf "%s" "${DOCKER_USER}:$(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "DOCKER_PASSWORD")" | base64 | tr -d '\n')

cat <<EOF >${_KANIKO_DOCKER_CONFIG_PATH}
{
  "auths": {
    "${DOCKER_REGISTRY:-https://index.docker.io/v1/}": {
      "auth": "${_TOKEN}"
    }
  }
}
EOF
