# Kaniko helpers scripts

- [kaniko/auth_with_docker.sh](#kanikoauth_with_dockersh)

## kaniko/auth_with_docker.sh

Authenticate on private [docker](https://www.docker.com/) registry using [kaniko](https://github.com/GoogleContainerTools/kaniko).

### Dependencies

- base64

### Environment variables

| Variable                                       | Required | Description                                                      |
|:-----------------------------------------------|:---------|:-----------------------------------------------------------------|
| `DOCKER_PASSWORD` or <br>`DOCKER_PASSWORD_B64` | Yes      | Docker private registry password                                 |
| `DOCKER_USER`                                  | Yes      | Docker private registry username                                 |
| `DOCKER_REGISTRY`                               | No      | Docker registry URL.<br>Defaults to `https://index.docker.io/v1/` |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    DOCKER_USER: "my-user"
    DOCKER_REGISTRY: "https://hub.docker.local"
  script:
    - ${CIH_PATH}/kaniko/auth_with_docker.sh
```
