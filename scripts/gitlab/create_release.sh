#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "GITLAB_TOKEN"
sh "${_CIH_PATH}/utils/require_variable.sh" "CI_SERVER_HOST"
sh "${_CIH_PATH}/utils/require_variable.sh" "CI_PROJECT_ID"
sh "${_CIH_PATH}/utils/require_variable.sh" "GITLAB_RELEASE_NAME"
sh "${_CIH_PATH}/utils/require_variable.sh" "GITLAB_RELEASE_TAG_NAME"
sh "${_CIH_PATH}/utils/require_variable.sh" "GITLAB_RELEASE_DESCRIPTION"

assets=",\"assets\":{\"links\":["

i=1
while [ "${i}" -ne 4 ]
do
    link_var="GITLAB_RELEASE_LINK${i}"
    if [ -n "$(eval echo \$"${link_var}")" ]; then
        link_name=$(eval echo "\$${link_var}" | cut -d '|' -f1)
        link_url=$(eval echo "\$${link_var}" | cut -d '|' -f2)
        link="{\"name\":\"${link_name}\", \"url\":\"${link_url}\"}"
        assets="${assets}${link},"
        i=$((i + 1))
    else
        break;
    fi
done

if [ "${i}" -gt 1 ]; then
    assets=$(echo "${assets}" | rev | cut -c2- | rev)
    assets="${assets}]}"
elif [ "${i}" -eq 1 ]; then
    unset assets
fi

_INPUT_JSON=$(cat << EOF
{
  "name": "${GITLAB_RELEASE_NAME}",
  "tag_name": "${GITLAB_RELEASE_TAG_NAME}",
  "description": "${GITLAB_RELEASE_DESCRIPTION}"${assets}
}
EOF
)

curl -qsSf \
     -X POST "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/releases" \
     -H "Content-Type:application/json" \
     -H "PRIVATE-TOKEN:$(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "GITLAB_TOKEN")" \
     -d "${_INPUT_JSON}"
