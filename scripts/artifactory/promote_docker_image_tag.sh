#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "ARTIFACTORY_TOKEN"
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_URL"
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_DOCKER_REPO"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_IMAGE"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_SRC_TAG"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_DEST_TAG"

_INPUT_JSON=$(cat << EOF
{
  "targetRepo": "${ARTIFACTORY_DOCKER_REPO}",
  "dockerRepository": "${DOCKER_IMAGE}",
  "tag": "${DOCKER_PROMOTE_SRC_TAG}",
  "targetTag": "${DOCKER_PROMOTE_DEST_TAG}",
  "copy": "true"
}
EOF
)

curl -qsSf \
     -X POST "${ARTIFACTORY_URL}/artifactory/api/docker/${ARTIFACTORY_DOCKER_REPO}/v2/promote" \
     -H "Content-Type:application/json" \
     -H "X-JFrog-Art-Api: $(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "ARTIFACTORY_TOKEN")" \
     -d "${_INPUT_JSON}"
