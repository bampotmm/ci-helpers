#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "ROCKETCHAT_TOKEN"
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "ROCKETCHAT_USER_ID"
sh "${_CIH_PATH}/utils/require_variable.sh" "ROCKETCHAT_URL"
sh "${_CIH_PATH}/utils/require_variable.sh" "ROCKETCHAT_CHANNEL"
sh "${_CIH_PATH}/utils/require_variable.sh" "ROCKETCHAT_MESSAGE"

_INPUT_JSON=$(cat << EOF
{
    "channel": "${ROCKETCHAT_CHANNEL}",
    "text": "${ROCKETCHAT_MESSAGE}"
}
EOF
)

curl -qsSf \
     -X POST "${ROCKETCHAT_URL}/api/v1/chat.postMessage" \
     -H "Content-Type:application/json" \
     -H "X-Auth-Token:$("${_CIH_PATH}/utils/get_secret_variable.sh" "ROCKETCHAT_TOKEN")" \
     -H "X-User-Id:$("${_CIH_PATH}/utils/get_secret_variable.sh" "ROCKETCHAT_USER_ID")" \
     -d "${_INPUT_JSON}"
