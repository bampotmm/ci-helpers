# RocketChat helpers scripts

- [rocketchat/send_message.sh](#rocketchatsend_messagesh)

## rocketchat/send_message.sh

Send message using [rocketchat](https://rocket.chat/).

### Dependencies

- base64
- curl

### Environment variables

| Variable                                             | Required | Description                                                      |
|:-----------------------------------------------------|:---------|:-----------------------------------------------------------------|
| `ROCKETCHAT_TOKEN` or <br>`ROCKETCHAT_TOKEN_B64`     | Yes      | RocketChat api key                                               |
| `ROCKETCHAT_USER_ID` or <br>`ROCKETCHAT_USER_ID_B64` | Yes      | RocketChat user id                                               |
| `ROCKETCHAT_URL`                                     | Yes      | RocketChat URL                                                   |
| `ROCKETCHAT_CHANNEL`                                 | Yes      | RocketChat destination channel                                   |
| `ROCKETCHAT_MESSAGE`                                 | Yes      | RocketChat message                                               |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    ROCKETCHAT_URL: "https://my.rocket.chat"
    ROCKETCHAT_CHANNEL: "#my-chan"
    ROCKETCHAT_MESSAGE: "Hello 🤙"
  script:
    - ${CIH_PATH}/rocketchat/send_message.sh
```
