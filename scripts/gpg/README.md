# GPG helpers scripts

- [gpg/import_private_key.sh](#gpgimport_private_keysh)
- [gpg/unlock_private_key.sh](#gpgunlock_private_keysh)

## gpg/import_private_key.sh

Import a private key with [GPG](https://gnupg.org/).

### Dependencies

- base64
- gpg

### Environment variables

| Variable                                           | Required | Description                           |
|:---------------------------------------------------|:---------|:--------------------------------------|
| `GPG_PRIVATE_KEY` or <br>`GPG_PRIVATE_KEY_B64`     | Yes      | GPG private key                       |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/gpg/import_private_key.sh
```

## gpg/unlock_private_key.sh

Unlock [GPG](https://gnupg.org/) private key to prevent typing passphrase.

### Dependencies

- base64
- gpg

### Environment variables

| Variable                                     | Required | Description                           |
|:---------------------------------------------|:---------|:--------------------------------------|
| `GPG_PASSPHRASE` or <br>`GPG_PASSPHRASE_B64` | Yes      | GPG private key passphrase            |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/gpg/unlock_private_key.sh
```
