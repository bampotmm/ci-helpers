#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "GPG_PRIVATE_KEY"

sh "${_CIH_PATH}/utils/get_secret_variable.sh" "GPG_PRIVATE_KEY" | gpg --batch --import

