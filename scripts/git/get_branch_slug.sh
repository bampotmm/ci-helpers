#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

if [ -n "${CI_COMMIT_REF_SLUG}" ]; then
  _SLUG_BRANCH="${CI_COMMIT_REF_SLUG}"
else
  _BRANCH="$(sh "${_CIH_PATH}/git/get_branch.sh")"
  _SLUG_BRANCH="$(sh "${_CIH_PATH}/utils/slugify.sh" "${_BRANCH}")"
fi

if [ -z "${BRANCH_SLUG_MAX_LENGTH}" ]; then
  BRANCH_SLUG_MAX_LENGTH="$(printf "%s" "${BRANCH_SLUG_PREFIX}${_SLUG_BRANCH}" | wc -c)"
fi

printf "%.*s" "${BRANCH_SLUG_MAX_LENGTH}" "${BRANCH_SLUG_PREFIX}${_SLUG_BRANCH}"
