#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_BRANCH=$(git rev-parse --abbrev-ref HEAD)

if [ "${_BRANCH}" != "HEAD" ]; then
  printf "%s" "${_BRANCH}"
fi
