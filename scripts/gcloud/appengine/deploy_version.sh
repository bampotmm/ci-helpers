#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_VERSION"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_DEPLOYABLES_PATH"

sed -i -r "s/service: (.*)/service: ${GAE_SERVICE:-default}/g" "${GAE_DEPLOYABLES_PATH}/app.yaml"

if [ -n "${GAE_PROMOTE_VERSION}" ]; then
  _GAE_PROMOTE="--promote"
else
  _GAE_PROMOTE="--no-promote"
fi

gcloud app deploy "${GAE_DEPLOYABLES_PATH}" \
                  --version "${GAE_VERSION}" \
                  "${_GAE_PROMOTE}" \
                  --project "${GCLOUD_PROJECT}"
