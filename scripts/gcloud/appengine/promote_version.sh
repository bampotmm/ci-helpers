#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_VERSION"

gcloud app services set-traffic "${GAE_SERVICE:-default}" \
                                --splits "${GAE_VERSION}=1" \
                                --project "${GCLOUD_PROJECT}"
