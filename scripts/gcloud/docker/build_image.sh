#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GCR_TAG"

gcloud builds submit "${GCR_SOURCE:-.}" \
                     --tag "${GCR_TAG}" \
                     --project "${GCLOUD_PROJECT}"
