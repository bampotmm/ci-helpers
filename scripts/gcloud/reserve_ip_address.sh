#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_IP_ADDRESS_NAME"

if [ -n "${GCLOUD_IP_ADDRESS_REGION}" ]; then
  _SET_REGION="--region=${GCLOUD_IP_ADDRESS_REGION}"
else
  _SET_REGION="--global"
fi

gcloud compute addresses create "${GCLOUD_IP_ADDRESS_NAME}" \
                                  "${_SET_REGION}" \
                                  --project "${GCLOUD_PROJECT}" --format="value(address)"
