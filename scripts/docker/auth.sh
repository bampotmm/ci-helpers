#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "DOCKER_PASSWORD"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_USER"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_REGISTRY"

sh "${_CIH_PATH}/utils/get_secret_variable.sh" "DOCKER_PASSWORD" | docker login "${DOCKER_REGISTRY}" --username "${DOCKER_USER}" --password-stdin
