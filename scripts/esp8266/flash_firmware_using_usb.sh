#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_FLASH_DEVICE"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_FLASH_FIRMWARE_PATH"

esptool.py -p "${ESP8266_FLASH_DEVICE}" write_flash 0x00000 "${ESP8266_FLASH_FIRMWARE_PATH}"
