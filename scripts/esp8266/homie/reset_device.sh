#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

if [ -n "${ESP8266_HOMIE_CONFIG_PATH}" ] && [ -f "${ESP8266_HOMIE_CONFIG_PATH}" ]; then
  ${CIH_PATH}/utils/log/info.sh "Homie config file detected, MQTT config will be read from it..."
  export MQTT_HOST=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.mqtt.host')
  export ESP8266_HOMIE_MQTT_BASE_TOPIC=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.mqtt.base_topic')
  export ESP8266_HOMIE_DEVICE_ID=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.device_id')
fi

sh "${_CIH_PATH}/utils/require_variable.sh" "MQTT_HOST"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_HOMIE_MQTT_BASE_TOPIC"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_HOMIE_DEVICE_ID"

mosquitto_pub -h "${MQTT_HOST}" -t "${ESP8266_HOMIE_BASE_TOPIC}/${ESP8266_HOMIE_DEVICE_ID}/\$implementation/reset" -m true
