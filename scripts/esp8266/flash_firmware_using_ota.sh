#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_FLASH_IP"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_FLASH_FIRMWARE_PATH"

espota.py -i "${ESP8266_FLASH_IP}" -f "${ESP8266_FLASH_FIRMWARE_PATH}"
