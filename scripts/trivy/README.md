# Trivy helpers scripts

- [trivy/scan_docker_image.sh](#trivyscan_docker_imagesh)

## trivy/scan_docker_image.sh

Scan an existing [docker](https://www.docker.com/) image using [trivy](https://github.com/aquasecurity/trivy) scanner.

### Dependencies

- base64
- trivy

### Environment variables

| Variable           | Required | Description                         |
|:-------------------|:---------|:------------------------------------|
| `DOCKER_IMAGE`     | Yes      | Docker image                        |
| `DOCKER_TAG`       | No       | Docker tag.<br>Defaults to `latest` |
| `DOCKER_REGISTRY`  | No       | Docker registry                     |

### Arguments

_Script has no arguments_

### Output

Print trivy vulnerabilities table.

### Exit codes

| Code   | Description                                           |
|:-------|:------------------------------------------------------|
| `0`    | Success                                               |
| `1`    | Failure                                               |
| `2`    | MEDIUM, HIGH or CRITICAL vulnerabilities are detected |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    DOCKER_IMAGE: "my-image"
    DOCKER_TAG: "1.0.0"
    DOCKER_REGISTRY: "docker.repo.local"
  script:
    - ${CIH_PATH}/trivy/scan_docker_image.sh
```
