SHELL := /bin/bash

check:
	docker run --rm -it -v $(shell pwd)/scripts:/src koalaman/shellcheck-alpine:stable /bin/sh -c "find /src -type f -name *.sh -print0 | xargs -0 shellcheck"

test:
	docker run --rm -it -v $(shell pwd):/src shellspec/shellspec:kcov --kcov --force-color -f d --random examples

release:
	npx standard-version --releaseCommitMessageFormat "chore(release): release version %s"
