# shellcheck shell=sh

Describe 'utils/log/error.sh'
  CIH_SCRIPT="${CIH_PATH}/utils/log/error.sh"

  It 'should success and print an error message'
    export EXPECTED_LOG_MESSAGE="my error message"
    export EXPECTED_ISO_DATE="2020-02-28T07:32:48Z"

    Intercept begin
      __begin__() {
        date() {
          case "$*" in
            "--utc +%FT%TZ")
              echo "${EXPECTED_ISO_DATE}"
          esac
        }
      }

    When run source "${CIH_SCRIPT}" "${EXPECTED_LOG_MESSAGE}"

    The status should be failure

    The error should include "${EXPECTED_ISO_DATE}"
    The error should include "[ERROR]"
    The error should include "${EXPECTED_LOG_MESSAGE}"
  End

  It 'should success and print an error message with a custom date format'
    export EXPECTED_LOG_MESSAGE="my error message"
    export CIH_LOG_DATE_FORMAT="+%m-%d-%y"
    export EXPECTED_DATE="02-28-20"

    Intercept begin
      __begin__() {
        date() {
          case "$*" in
            "--utc ${CIH_LOG_DATE_FORMAT}")
              echo "02-28-20"
          esac
        }
      }

    When run source "${CIH_SCRIPT}" "${EXPECTED_LOG_MESSAGE}"

    The status should be failure

    The error should include "02-28-20"
    The error should include "[ERROR]"
    The error should include "${EXPECTED_LOG_MESSAGE}"
  End
End
