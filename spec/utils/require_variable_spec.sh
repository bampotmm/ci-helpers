# shellcheck shell=sh

Describe 'utils/require_variable.sh'
  CIH_SCRIPT="${CIH_PATH}/utils/require_variable.sh"

  It 'should fail when variable is not defined'
    unset FOO

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO environment variable should be defined"
  End

  # ---

  It 'should success when variable is defined'
    export FOO="foo"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
  End
End
