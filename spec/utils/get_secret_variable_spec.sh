# shellcheck shell=sh

Describe 'utils/get_secret_variable.sh'
  CIH_SCRIPT="${CIH_PATH}/utils/get_secret_variable.sh"

  It 'should success and decode content of encoded variable'
    export FOO_B64="Zm9vYmFy"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
    The output should include "foobar"
  End

  It 'should success and not decode content of variable'
    export FOO="foobar"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
    The output should include "foobar"
  End
End
