# shellcheck shell=sh

Describe 'utils/require_secret_variable.sh'
  CIH_SCRIPT="${CIH_PATH}/utils/require_secret_variable.sh"

  It 'should fail when none of variable and encoded variable are defined'
    unset FOO
    unset FOO_B64

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO or FOO_B64 environment variable should be defined"
  End

  # ---

  It 'should success when encoded variable is defined and its content is maskable'
    export FOO_B64="Zm9vYmFyYmF6"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
  End

  # ---

  It 'should fail when encoded variable is defined and its content is not maskable'
    export FOO_B64="foo"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO_B64 environment variable value should be maskable by Gitlab. More information here : https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
  End

  # ---

  It 'should warn when both variable and encoded variable are defined'
    export FOO="foobarbaz"
    export FOO_B64="Zm9vYmFyYmF6"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
    The output should include "both FOO and FOO_B64 environment variables are defined, FOO_B64 will be used by default"
  End

  # ---

  It 'should warn when variable is defined and its content could be masked'
    export FOO="foo"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
    The output should include "FOO environment variable can't be masked by Gitlab. Consider replace it by the FOO_B64 environment variable with base64 encoded value due to safety reasons. More information here: https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
  End

  # ---

  It 'should fail when encoded variable has multi lines content'
    export FOO_B64="foo
                    bar"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO_B64 environment variable value should be maskable by Gitlab. More information here : https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
  End

  # ---

  It 'should fail when encoded variable content has less than 8 characters'
    export FOO_B64="foobar!"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO_B64 environment variable value should be maskable by Gitlab. More information here : https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
  End

  # ---

  It 'should fail when encoded variable content does not consist only of the Base64 alphabet (RFC4648) including \"@\" and \":\"'
    export FOO_B64="foobarbaz$"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be failure
    The error should include "FOO_B64 environment variable value should be maskable by Gitlab. More information here : https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
  End

  # ---

  It 'should success when encoded variable content only consists of the Base64 alphabet (RFC4648) including \"@\" and \":\"'
    export FOO_B64="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ0123456789_-+=@:"

    When run source "${CIH_SCRIPT}" "FOO"

    The status should be success
  End
End
