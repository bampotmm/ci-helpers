# shellcheck shell=sh

Describe 'gpg/unlock_private_key.sh'
  CIH_SCRIPT="${CIH_PATH}/gpg/unlock_private_key.sh"

  set_variables() {
    export GPG_PASSPHRASE="my-passphrase"
  }

  Before 'set_variables'

  It 'should fail when GPG_PASSPHRASE env variable is not defined'
    unset GPG_PASSPHRASE

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GPG_PASSPHRASE or GPG_PASSPHRASE_B64 environment variable should be defined"
  End

 # ---

  It 'should success and unlock secret key'
    export _DUMMY_FILE_PATH=$(mktemp)

    Intercept begin
    __begin__() {
      gpg() {
        _INPUT="$(cat)"

        if [ "${_INPUT}" = "${GPG_PASSPHRASE}" ] && [ "${*}" = "--batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s ${_DUMMY_FILE_PATH}" ]; then
          echo "success"
        fi
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

 # ---

  It 'should fail on gpg error'
    Intercept begin
    __begin__() {
      gpg() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End

End
