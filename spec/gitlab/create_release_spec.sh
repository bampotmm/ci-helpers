# shellcheck shell=sh

Describe 'gitlab/create_release.sh'
  CIH_SCRIPT="${CIH_PATH}/gitlab/create_release.sh"

  set_variables() {
    export GITLAB_TOKEN="my-token"
    export CI_SERVER_HOST="gitlab.com"
    export CI_PROJECT_ID=666
    export GITLAB_RELEASE_NAME="v1.0.0"
    export GITLAB_RELEASE_TAG_NAME="1.0.0"
    export GITLAB_RELEASE_DESCRIPTION="my release description"
  }

  Before 'set_variables'

  It 'should fail when GITLAB_TOKEN env variable is not defined'
    unset GITLAB_TOKEN

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GITLAB_TOKEN or GITLAB_TOKEN_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when CI_SERVER_HOST env variable is not defined'
    unset CI_SERVER_HOST

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "CI_SERVER_HOST environment variable should be defined"
  End

  # ---

  It 'should fail when CI_PROJECT_ID env variable is not defined'
    unset CI_PROJECT_ID

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "CI_PROJECT_ID environment variable should be defined"
  End

  # ---

  It 'should fail when GITLAB_RELEASE_NAME env variable is not defined'
    unset GITLAB_RELEASE_NAME

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GITLAB_RELEASE_NAME environment variable should be defined"
  End

  # ---

  It 'should fail when GITLAB_RELEASE_TAG_NAME env variable is not defined'
    unset GITLAB_RELEASE_TAG_NAME

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GITLAB_RELEASE_TAG_NAME environment variable should be defined"
  End

  # ---

  It 'should fail when GITLAB_RELEASE_DESCRIPTION env variable is not defined'
    unset GITLAB_RELEASE_DESCRIPTION

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GITLAB_RELEASE_DESCRIPTION environment variable should be defined"
  End

  # ---

  It 'should success and create release with no defined links'
    EXPECTED_INPUT_JSON=$(cat << EOF
{
  "name": "${GITLAB_RELEASE_NAME}",
  "tag_name": "${GITLAB_RELEASE_TAG_NAME}",
  "description": "${GITLAB_RELEASE_DESCRIPTION}"
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/releases -H Content-Type:application/json -H PRIVATE-TOKEN:${GITLAB_TOKEN} -d ${EXPECTED_INPUT_JSON}")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success and create release with one link'
    export GITLAB_RELEASE_LINK1="foo|https://foo.com"
    EXPECTED_INPUT_JSON=$(cat << EOF
{
  "name": "${GITLAB_RELEASE_NAME}",
  "tag_name": "${GITLAB_RELEASE_TAG_NAME}",
  "description": "${GITLAB_RELEASE_DESCRIPTION}","assets":{"links":[{"name":"foo", "url":"https://foo.com"}]}
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/releases -H Content-Type:application/json -H PRIVATE-TOKEN:${GITLAB_TOKEN} -d ${EXPECTED_INPUT_JSON}")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success and create release with two links'
    export GITLAB_RELEASE_LINK1="foo|https://foo.com"
    export GITLAB_RELEASE_LINK2="bar|https://bar.com"

    EXPECTED_INPUT_JSON=$(cat << EOF
{
  "name": "${GITLAB_RELEASE_NAME}",
  "tag_name": "${GITLAB_RELEASE_TAG_NAME}",
  "description": "${GITLAB_RELEASE_DESCRIPTION}","assets":{"links":[{"name":"foo", "url":"https://foo.com"},{"name":"bar", "url":"https://bar.com"}]}
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/releases -H Content-Type:application/json -H PRIVATE-TOKEN:${GITLAB_TOKEN} -d ${EXPECTED_INPUT_JSON}")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success and create release with three links'
    export GITLAB_RELEASE_LINK1="foo|https://foo.com"
    export GITLAB_RELEASE_LINK2="bar|https://bar.com"
    export GITLAB_RELEASE_LINK3="baz|https://baz.com"

    EXPECTED_INPUT_JSON=$(cat << EOF
{
  "name": "${GITLAB_RELEASE_NAME}",
  "tag_name": "${GITLAB_RELEASE_TAG_NAME}",
  "description": "${GITLAB_RELEASE_DESCRIPTION}","assets":{"links":[{"name":"foo", "url":"https://foo.com"},{"name":"bar", "url":"https://bar.com"},{"name":"baz", "url":"https://baz.com"}]}
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/releases -H Content-Type:application/json -H PRIVATE-TOKEN:${GITLAB_TOKEN} -d ${EXPECTED_INPUT_JSON}")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on curl error'
    Intercept begin
    __begin__() {
      curl() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
