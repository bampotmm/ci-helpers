# shellcheck shell=sh

Describe 'artifactory/add_tag_to_docker_image.sh'
  CIH_SCRIPT="${CIH_PATH}/artifactory/add_tag_to_docker_image.sh"

  set_variables() {
    export ARTIFACTORY_TOKEN="my-token"
    export ARTIFACTORY_URL="https://repo.artifactory.com"
    export ARTIFACTORY_DOCKER_IMAGE="my-image"
    export ARTIFACTORY_DOCKER_REPO="docker-local"
    export ARTIFACTORY_DOCKER_SRC_TAG="my-tag"
    export ARTIFACTORY_DOCKER_DEST_TAG="my-new-tag"
  }

  Before 'set_variables'

  It 'should fail when ARTIFACTORY_TOKEN env variable is not defined'
    unset ARTIFACTORY_TOKEN

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_TOKEN or ARTIFACTORY_TOKEN_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_URL env variable is not defined'
    unset ARTIFACTORY_URL

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_URL environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_DOCKER_IMAGE env variable is not defined'
    unset ARTIFACTORY_DOCKER_IMAGE

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_DOCKER_IMAGE environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_DOCKER_REPO env variable is not defined'
    unset ARTIFACTORY_DOCKER_REPO

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_DOCKER_REPO environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_DOCKER_SRC_TAG env variable is not defined'
    unset ARTIFACTORY_DOCKER_SRC_TAG

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_DOCKER_SRC_TAG environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_DOCKER_DEST_TAG env variable is not defined'
    unset ARTIFACTORY_DOCKER_DEST_TAG

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_DOCKER_DEST_TAG environment variable should be defined"
  End

  # ---

  It 'should success and promote tag'
    EXPECTED_INPUT_JSON=$(cat << EOF
{
  "targetRepo": "${ARTIFACTORY_DOCKER_REPO}",
  "dockerRepository": "${ARTIFACTORY_DOCKER_IMAGE}",
  "tag": "${ARTIFACTORY_DOCKER_SRC_TAG}",
  "targetTag": "${ARTIFACTORY_DOCKER_DEST_TAG}",
  "copy": "true"
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST ${ARTIFACTORY_URL}/api/docker/${ARTIFACTORY_DOCKER_REPO}/v2/promote -H Content-Type:application/json -H X-JFrog-Art-Api: ${ARTIFACTORY_TOKEN} -d ${EXPECTED_INPUT_JSON}")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on curl error'
    Intercept begin
    __begin__() {
      curl() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
