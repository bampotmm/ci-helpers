# shellcheck shell=sh

Describe 'artifactory/auth_with_npm.sh'
  CIH_SCRIPT="${CIH_PATH}/artifactory/auth_with_npm.sh"

  set_variables() {
    export ARTIFACTORY_TOKEN="my-token"
    export ARTIFACTORY_URL="https://repo.artifactory.com"
    export ARTIFACTORY_NPM_REPO="npm-local"
  }

  Before 'set_variables'

  It 'should fail when ARTIFACTORY_TOKEN env variable is not defined'
    unset ARTIFACTORY_TOKEN

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_TOKEN or ARTIFACTORY_TOKEN_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_URL env variable is not defined'
    unset ARTIFACTORY_URL

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_URL environment variable should be defined"
  End

  # ---

  It 'should fail when ARTIFACTORY_NPM_REPO env variable is not defined'
    unset ARTIFACTORY_NPM_REPO

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ARTIFACTORY_NPM_REPO environment variable should be defined"
  End

  # ---

  It 'should success and authenticates NPM with artifactory'
    export NPM_RC_PATH="${SHELLSPEC_TMPDIR}/.npmrc"
    EXPECTED_NPM_RC_CONTENT=$(cat << EOF
_auth=my-npm-token
always-auth=true
email=foo@bar.com
registry=${ARTIFACTORY_URL}/api/npm/${ARTIFACTORY_NPM_REPO}/
EOF
)

    Intercept begin
    __begin__() {
      npm() {
        case "$*" in
          "config set registry ${ARTIFACTORY_URL}/api/npm/${ARTIFACTORY_NPM_REPO}/")
            touch "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_npm_has_been_called"
        esac
      }

      curl() {
        case "$*" in
          "-qsSf -X GET ${ARTIFACTORY_URL}/api/npm/auth -H X-Api-Key: ${ARTIFACTORY_TOKEN}")
            echo "${EXPECTED_NPM_RC_CONTENT}"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The file "${NPM_RC_PATH}" contents should equal "${EXPECTED_NPM_RC_CONTENT}"
    The file "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_npm_has_been_called" should be file

    rm -rf "${NPM_RC_PATH}"
    rm -rf "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_npm_has_been_called"

  End

  # ---

  It 'should fail on curl error'
    export NPM_RC_PATH="${SHELLSPEC_TMPDIR}/.npmrc"

    Intercept begin
    __begin__() {
      curl() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"

    rm -rf "${NPM_RC_PATH}"
  End
End
