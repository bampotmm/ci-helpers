# shellcheck shell=sh

Describe 'gcloud/appengine/is_version_has_traffic.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/appengine/is_version_has_traffic.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GAE_VERSION="my-version"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GAE_VERSION env variable is not defined'
    unset GAE_VERSION

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GAE_VERSION environment variable should be defined"
  End

  # ---

  It 'should success when all traffic is forwarded on version'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app versions list --project ${GCLOUD_PROJECT} --service default --filter ${GAE_VERSION} --hide-no-traffic --format=value(traffic_split)")
            echo "1.00"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
  End

  # ---

  It 'should success when all traffic is forwarded on version with specified service'
    export GAE_SERVICE="my-service"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app versions list --project ${GCLOUD_PROJECT} --service ${GAE_SERVICE} --filter ${GAE_VERSION} --hide-no-traffic --format=value(traffic_split)")
            echo "1.00"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
  End

  # ---

  It 'should fail when no traffic is forwarded on version'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app versions list --project ${GCLOUD_PROJECT} --service default --filter ${GAE_VERSION} --hide-no-traffic --format=value(traffic_split)")
            echo "0.00"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
