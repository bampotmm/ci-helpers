# shellcheck shell=sh

Describe 'gcloud/appengine/delete_version.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/appengine/delete_version.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GAE_VERSION="my-version"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GAE_VERSION env variable is not defined'
    unset GAE_VERSION

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GAE_VERSION environment variable should be defined"
  End

  # ---

  It 'should success and delete desired version on default service when GAE_SERVICE environment variable is not specified'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app versions delete --service default ${GAE_VERSION} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success and delete desired version on specific service when GAE_SERVICE environment variable is specified'
    export GAE_SERVICE="my-service"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app versions delete --service my-service ${GAE_VERSION} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
