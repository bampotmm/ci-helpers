# shellcheck shell=sh

Describe 'gcloud/appengine/is_service_exists.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/appengine/is_service_exists.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GAE_SERVICE="my-service"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GAE_SERVICE env variable is not defined'
    unset GAE_SERVICE

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GAE_SERVICE environment variable should be defined"
  End

  # ---

  It 'should success when service is found'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app services list --project ${GCLOUD_PROJECT} --filter=${GAE_SERVICE} --format=value(id)")
            echo "${GAE_SERVICE}"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
  End

  # ---

  It 'should fail when service is not found'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app services list --project ${GCLOUD_PROJECT} --filter=${GAE_SERVICE} --format=value(id)")
            echo "foo"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
