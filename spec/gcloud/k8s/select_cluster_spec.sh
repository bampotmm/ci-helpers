# shellcheck shell=sh

Describe 'gcloud/k8s/select_cluster.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/k8s/select_cluster.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export K8S_CLUSTER="my-cluster"
    export K8S_CLUSTER_ZONE="europe-west1c"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when K8S_CLUSTER env variable is not defined'
    unset K8S_CLUSTER

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "K8S_CLUSTER environment variable should be defined"
  End

  # ---

  It 'should fail when K8S_CLUSTER_ZONE env variable is not defined'
    unset K8S_CLUSTER_ZONE

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "K8S_CLUSTER_ZONE environment variable should be defined"
  End

  # ---

  It 'should success when cluster name and zone are defined'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "container clusters get-credentials ${K8S_CLUSTER} --zone ${K8S_CLUSTER_ZONE} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
