# shellcheck shell=sh

Describe 'gcloud/docker/build_image.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/docker/build_image.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GCR_TAG="gcr.io/${GCLOUD_PROJECT}/my-image:tag"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GCR_TAG env variable is not defined'
    unset GCR_TAG

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCR_TAG environment variable should be defined"
  End

  # ---

  It 'should success when building image with no source specified'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "builds submit . --tag ${GCR_TAG} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success when building image with a specified source'
    export GCR_SOURCE="docker"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "builds submit ${GCR_SOURCE} --tag ${GCR_TAG} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
