# shellcheck shell=sh

Describe 'yarn/install_dependencies.sh'
  CIH_SCRIPT="${CIH_PATH}/yarn/install_dependencies.sh"

  It 'should success and start dependencies install'
    Intercept begin
    __begin__() {
      yarn() {
         case "$*" in
            "--frozen-lockfile")
              echo "success"
          esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End
End
